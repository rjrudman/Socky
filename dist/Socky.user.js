// ==UserScript==
// @name         Socky
// @namespace    https://gitlab.com/rjrudman/Socky
// @version      0.1.3
// @author       Robert Rudman
// @match        *://*.stackexchange.com/*
// @match        *://*.stackoverflow.com/*
// @match        *://*.superuser.com/*
// @match        *://*.serverfault.com/*
// @match        *://*.askubuntu.com/*
// @match        *://*.stackapps.com/*
// @match        *://*.mathoverflow.net/*
// @exclude      *://chat.stackexchange.com/*
// @exclude      *://chat.meta.stackexchange.com/*
// @exclude      *://chat.stackoverflow.com/*
// @exclude      *://blog.stackoverflow.com/*
// @exclude      *://*.area51.stackexchange.com/*
// @require      https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.4/require.min.js
// @grant        GM_xmlhttpRequest
// ==/UserScript==
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    // https://stackoverflow.com/a/8578840/563532
    function AddScript(url, callback) {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.async = true;
        script.onload = function () {
            // remote script has loaded
            if (callback) {
                callback();
            }
        };
        script.src = url;
        document.getElementsByTagName('head')[0].appendChild(script);
    }
    exports.AddScript = AddScript;
    function AddStyle(url) {
        var head = document.head || document.getElementsByTagName('head')[0];
        var style = document.createElement('link');
        style.rel = 'stylesheet';
        style.type = 'text/css';
        style.href = url;
        head.appendChild(style);
    }
    exports.AddStyle = AddStyle;
    function AddStyleText(text) {
        var head = document.head || document.getElementsByTagName('head')[0];
        var style = document.createElement('style');
        style.type = 'text/css';
        if (style.styleSheet) {
            style.styleSheet.cssText = text;
        }
        else {
            style.appendChild(document.createTextNode(text));
        }
        head.appendChild(style);
    }
    exports.AddStyleText = AddStyleText;
    function Sum(data, selector) {
        return data.reduce(function (previous, current) { return previous + selector(current); }, 0);
    }
    exports.Sum = Sum;
    function Max(data, selector) {
        var result = data.reduce(function (previous, current) {
            var currentValue = selector(current);
            if (previous === undefined || previous < currentValue) {
                return previous;
            }
            return currentValue;
        }, undefined);
        if (result === undefined) {
            return null;
        }
        return result;
    }
    exports.Max = Max;
    // https://stackoverflow.com/a/34890276/563532
    function GroupBy(data, key) {
        return data.reduce(function (rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
        }, {});
    }
    exports.GroupBy = GroupBy;
    function Delay(time) {
        return new Promise(function (resolve) {
            setTimeout(function () {
                resolve();
            }, time);
        });
    }
    exports.Delay = Delay;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(2), __webpack_require__(3), __webpack_require__(0)], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, tslib_1, UserActivity_1, Tools_1) {
    "use strict";
    var _this = this;
    Object.defineProperty(exports, "__esModule", { value: true });
    var sockyCss = "\n#sockyTable > thead > tr > th {\n    background-color: #6a737c;\n    color: #FFF;\n    border: 1px solid #FFF;\n    font-size: 110%;\n    padding: 5px;\n}\n\n#sockyTable > tbody > tr > td {\n    padding: 5px;\n    vertical-align: middle;\n}\n#sockyTable > tbody > tr:nth-child(even) {\n    background-color: #3332320d;\n}\n\n";
    function setupSockyLink() {
        var match = window.location.href.match(/\/users\/account\-info\/(\d+)/);
        if (match) {
            var userId_1 = match[1];
            $('.mod-quick-links')
                .append($('<li><a href="/users/account-info/' + userId_1 + '#Socky">Socky</a></li>')
                .click(function () {
                // If we're already browsing socky, don't trigger a reload.
                if (!window.location.href.match(/\/users\/account\-info\/(\d+)#Socky/)) {
                    displaySockyUI(userId_1, $('.mod-tabs').text().trim());
                }
            }));
        }
    }
    function addSockyCss() {
        Tools_1.AddStyleText(sockyCss);
        $('.left-sidebar').hide();
        $('#content').css('width', '100%');
        $('#mod-content').css('width', '100%');
        $('#mod-content').css('max-width', '100%');
    }
    function displaySockyUI(pageUserId, userName) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            var contentBox, content, userIdsToCheck, processAddUser, contentBody, observer, reputationInfo;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        contentBox = $('#mod-content .col-9');
                        addSockyCss();
                        contentBox.empty();
                        content = $("\n    <div class=\"subheader\" style=\"height: 100%\">\n        <h1 style=\"float: none;\">\n            Socky information for <a href=\"/users/" + pageUserId + "\">" + userName + "</a>\n        </h1>\n        <h2 id=\"warnings\" style=\"display: none; float: none; margin-top: 5px; background-color: #efa812; padding: 5px;\"></h2>\n    </div>\n    <table id=\"sockyTable\" style=\"width: 100%\">\n        <thead>\n            <tr>\n                <th><input type=\"checkbox\" id=\"activitySelectAll\"></th>\n                <th width=\"215px\">User</th>\n                <th>Previous Names</th>\n                <th>Real Name</th>\n                <th>Email</th>\n                <th>IP</th>\n                <th>Votes given</th>\n                <th>Votes recieved</th>\n                <th title=\"will be logged\">PII \u26A0</th>\n            </tr>\n        </thead>\n        <tbody id=\"contentBody\">\n        </tbody>\n    </table>\n    <br />\n    <input type=\"button\" id=\"checkUserActivity\" value=\"Check Selected User's Activity\" style=\"cursor: pointer;\">\n    <br />\n    ");
                        contentBox.append(content);
                        userIdsToCheck = [];
                        processAddUser = function () {
                            var userIdToCheck = userIdsToCheck.pop();
                            if (userIdToCheck) {
                                $('#user').val(userIdToCheck);
                                $('#addUser').click();
                            }
                            else {
                                $('#checkUserActivity')
                                    .removeAttr('disabled')
                                    .removeClass('disabled-button');
                            }
                        };
                        $('#checkUserActivity').click(function () {
                            $('.socky-user-input:checked').each(function (i, elem) {
                                var aa = parseInt($(elem).attr('data-id'), 10);
                                userIdsToCheck.push(aa);
                            });
                            userIdsToCheck.reverse();
                            $('#refresh').click();
                            if (userIdsToCheck.length > 0) {
                                processAddUser();
                                $('#checkUserActivity').disable();
                            }
                            else {
                                alert('No users selected');
                            }
                        });
                        $('#activitySelectAll').click(function () {
                            if ($('#activitySelectAll').prop('checked')) {
                                $('.socky-user-input').prop('checked', true);
                            }
                            else {
                                $('.socky-user-input').prop('checked', false);
                            }
                        });
                        contentBody = $('#contentBody');
                        $.get("/admin/show-user-votes/" + pageUserId, function (data) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                            var _this = this;
                            var results, grouped, flattened, processAfter, _loop_1, flattened_1, flattened_1_1, item, i, e_1, _a;
                            return tslib_1.__generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        results = parseUserVotes(data, pageUserId, userName);
                                        grouped = Tools_1.GroupBy(results.Votes, 'UserId');
                                        flattened = flattenUserVotes(grouped, pageUserId);
                                        processAfter = [];
                                        _loop_1 = function (item) {
                                            var currentUserId = item.UserId;
                                            if (!currentUserId) {
                                                return "continue";
                                            }
                                            if (currentUserId === pageUserId) {
                                                item.UserImage.css('background-color', '#b5f1b5');
                                            }
                                            var userImage = $('<td>').append(item.UserImage);
                                            var previousNames = $('<td>');
                                            var realName = $('<td>');
                                            var email = $('<td>');
                                            var ip = $('<td>');
                                            var loadPII = $('<a href="javascript:void(0);">Load</a>');
                                            loadPII.click(function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                                                var pii;
                                                return tslib_1.__generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0:
                                                            loadPII.hide();
                                                            return [4 /*yield*/, FetchPII(currentUserId)];
                                                        case 1:
                                                            pii = _a.sent();
                                                            email.append(pii.email);
                                                            realName.append(pii.realName);
                                                            ip.html(pii.ip);
                                                            return [2 /*return*/];
                                                    }
                                                });
                                            }); });
                                            contentBody.append($('<tr>')
                                                .append("<td><input type=\"checkbox\" class=\"socky-user-input\" data-id=\"" + currentUserId + "\"></td>")
                                                .append(userImage)
                                                .append(previousNames)
                                                .append(realName)
                                                .append(email)
                                                .append(ip)
                                                .append($('<td>').html(item.VotesGiven))
                                                .append($('<td>').html(item.VotesRecieved))
                                                .append($('<td>').append(loadPII)));
                                            processAfter.push(function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                                                var dashboardData, joinedDate, userDetailsDiv;
                                                return tslib_1.__generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0: return [4 /*yield*/, Tools_1.Delay(1000)];
                                                        case 1:
                                                            _a.sent();
                                                            return [4 /*yield*/, FetchModDashboard(currentUserId)];
                                                        case 2:
                                                            dashboardData = _a.sent();
                                                            item.UserImage.css('float', 'left');
                                                            dashboardData.modLinks
                                                                .css('float', 'right')
                                                                .css('margin-left', '2px')
                                                                .css('margin-right', '-5px');
                                                            userImage.append(dashboardData.modLinks);
                                                            if (dashboardData.joinedSite) {
                                                                joinedDate = $("<p>Joined " + dashboardData.joinedSite + "</p>").css('margin-bottom', '0px');
                                                                userDetailsDiv = item.UserImage.find('.user-details');
                                                                if (userDetailsDiv.length > 0) {
                                                                    userDetailsDiv.append(joinedDate);
                                                                }
                                                                else {
                                                                    item.UserImage.parent().append(joinedDate);
                                                                }
                                                            }
                                                            dashboardData.pastNames.find('li').each(function (index, element) {
                                                                var newText = $('<p>').text($(element).text());
                                                                newText.css('margin-bottom', '0px');
                                                                previousNames.append(newText);
                                                            });
                                                            return [2 /*return*/];
                                                    }
                                                });
                                            }); });
                                        };
                                        try {
                                            for (flattened_1 = tslib_1.__values(flattened), flattened_1_1 = flattened_1.next(); !flattened_1_1.done; flattened_1_1 = flattened_1.next()) {
                                                item = flattened_1_1.value;
                                                _loop_1(item);
                                            }
                                        }
                                        catch (e_1_1) { e_1 = { error: e_1_1 }; }
                                        finally {
                                            try {
                                                if (flattened_1_1 && !flattened_1_1.done && (_a = flattened_1.return)) _a.call(flattened_1);
                                            }
                                            finally { if (e_1) throw e_1.error; }
                                        }
                                        if (results.HasRingleader) {
                                            content.find('#warnings')
                                                .show()
                                                .html("Ring voting detected: <a href=\"/admin/show-user-votes/" + pageUserId + "\">View votes</a>");
                                        }
                                        i = 0;
                                        _b.label = 1;
                                    case 1:
                                        if (!(i < processAfter.length)) return [3 /*break*/, 4];
                                        return [4 /*yield*/, processAfter[i]()];
                                    case 2:
                                        _b.sent();
                                        _b.label = 3;
                                    case 3:
                                        i++;
                                        return [3 /*break*/, 1];
                                    case 4: return [2 /*return*/];
                                }
                            });
                        }); });
                        UserActivity_1.BuildUserActivity(contentBox);
                        // Fix the permalink for user activity. Without this, it'll point to /users/account-info/userid/#...
                        $('.permalink > a').click(function () {
                            setTimeout(function () {
                                var permalink = $('.permalink input').eq(0);
                                var updatedLink = permalink.val().replace("/users/account-info/" + pageUserId, '/admin/user-activity');
                                permalink.val(updatedLink);
                                permalink.select();
                            }, 100);
                        });
                        observer = new MutationObserver(function (mutations) {
                            var mutation;
                            // tslint:disable-next-line:no-conditional-assignment
                            for (var i = 0; mutation = mutations[i]; i++) {
                                if (mutation.attributeName === 'disabled') {
                                    if (!mutation.target.disabled) {
                                        setTimeout(function () {
                                            processAddUser();
                                        }, 2000);
                                    }
                                }
                            }
                        });
                        // Observe attributes change
                        observer.observe($('#addUser').get(0), { attributes: true });
                        return [4 /*yield*/, FetchUserReputation(pageUserId)];
                    case 1:
                        reputationInfo = _a.sent();
                        contentBox.append(reputationInfo.Content);
                        return [2 /*return*/];
                }
            });
        });
    }
    function FetchPII(userId) {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/all-pii",
                type: 'POST',
                data: { fkey: StackExchange.options.user.fkey, id: userId },
                success: function (pii) {
                    var data = $('.col-4', pii);
                    var email = data.eq(0).find('a');
                    email.remove('svg');
                    var realName = data.eq(1).find('a');
                    realName.remove('svg');
                    var ip = data.eq(2).html();
                    resolve({
                        email: email,
                        realName: realName,
                        ip: ip
                    });
                },
                error: function (err) {
                    reject(err);
                }
            });
        });
    }
    function FetchModDashboard(userId) {
        return new Promise(function (resolve, reject) {
            $.get("/users/account-info/" + userId, function (data) {
                var jqueryPage = $(data);
                var results = parseModDashboard(jqueryPage);
                resolve(results);
            }).fail(function (err) {
                reject(err);
            });
        });
    }
    function parseModDashboard(page) {
        var modLinks = $('<span class="mod-links"></span>').append(page.find('.mod-flag-indicator'));
        var pastNames = page.find('.col-2:contains("Past names:")').next().find('ul');
        var joinedSite = page.find('.col-2:contains("Joined site:")').next().text();
        return { modLinks: modLinks, pastNames: pastNames, joinedSite: joinedSite };
    }
    function FetchUserReputation(userId) {
        var capturedResolve;
        var capturedReject;
        var promise = new Promise(function (resolve, reject) {
            capturedResolve = resolve;
            capturedReject = reject;
        });
        Tools_1.AddScript('https://cdn.sstatic.net/Js/third-party/highcharts.js', function () {
            $.get("/users/" + userId + "/?tab=reputation", function (data) {
                var jqueryPage = $(data);
                var results = parseReputationPage(jqueryPage);
                capturedResolve(results);
            }).fail(function (err) {
                capturedReject(err);
            });
        });
        return promise;
    }
    function parseReputationPage(page) {
        return { Content: page.find('#user-tab-reputation .user-tab-content') };
    }
    function flattenUserVotes(grouped, userId) {
        var flattened = Object.keys(grouped)
            .map(function (key) {
            var values = grouped[key];
            var userImage = values[0].UserInfo;
            var votingUserIds = values.filter(function (v) { return !!v.UserId; });
            var votingUserId = votingUserIds.length ?
                votingUserIds[0].UserId
                : null;
            var votesGiven = values.filter(function (v) { return v.Given > 0; })
                .reduce(function (previous, current, i) {
                var voteText = current.Type !== 'Invalidated'
                    ? current.Given + ' (' + (current.Total === 0 ? 0 : Math.round(current.Given * 100 / current.Total)) + '%) '
                    : current.Given + ' ';
                return previous + voteText + current.Type + (i === 0 ? '<br />' : '');
            }, '');
            var votesRecieved = values.filter(function (v) { return v.Recieved > 0; })
                .reduce(function (previous, current, i) {
                var voteText = current.Type !== 'Invalidated'
                    ? current.Recieved + ' (' + (current.Total === 0 ? 0 : Math.round(current.Recieved * 100 / current.Total)) + '%) '
                    : current.Recieved + ' ';
                return previous + voteText + current.Type + (i === 0 ? '<br />' : '');
            }, '');
            return {
                UserImage: userImage,
                UserId: votingUserId,
                PreviousName: null,
                RealName: null,
                Email: null,
                IP: null,
                VotesGiven: votesGiven,
                VotesRecieved: votesRecieved,
                TotalVotes: Tools_1.Sum(values, function (v) { return v.Given + v.Recieved; }),
            };
        });
        flattened.sort(function (a, b) {
            if (a.UserId === userId) {
                return -1;
            }
            if (b.UserId === userId) {
                return 1;
            }
            return b.TotalVotes - a.TotalVotes;
        });
        return flattened;
    }
    function parseUserVotes(page, userId, userName) {
        var grids = $('#mainbar-full > .d-flex', page);
        var parseUserDetails = function (e) {
            var element = $(e);
            var userInfo = element.find('.user-info');
            var currentUserId = element.attr('data-user-id');
            if (currentUserId === undefined) {
                // Deleted sock puppets don't have a data-user-id attribute
                var userLink = userInfo.find('.user-details > a').attr('href');
                var userIdMatch = userLink.match(/\/users\/(\d+)/);
                currentUserId = userLink && userIdMatch ? userIdMatch[1] : null;
            }
            return {
                UserId: currentUserId,
                UserInfo: userInfo
            };
        };
        var recievedVotes = grids.eq(0).find('table').eq(0).find('tbody > tr').toArray().map(function (e) {
            var element = $(e);
            var numColumns = element.find('td').length;
            var type = element.find('td').eq(numColumns === 5 ? 1 : 0).find('span').text();
            var votes = element.find('td').eq(numColumns === 5 ? 2 : 1).text();
            var recieved = parseInt(votes.split(' / ')[0], 10);
            var total = parseInt(votes.split(' / ')[1], 10);
            return tslib_1.__assign({ Type: type, Recieved: recieved, Given: 0, Total: total }, parseUserDetails(e));
        });
        var givenVotes = grids.eq(0).find('table').eq(1).find('tbody > tr').toArray().map(function (e) {
            var element = $(e);
            var numColumns = element.find('td').length;
            var type = element.find('td').eq(numColumns === 5 ? 1 : 0).find('span').text();
            var votes = element.find('td').eq(numColumns === 5 ? 2 : 1).text();
            var given = parseInt(votes.split(' / ')[0], 10);
            var total = parseInt(votes.split(' / ')[1], 10);
            return tslib_1.__assign({ Type: type, Recieved: 0, Given: given, Total: total }, parseUserDetails(e));
        });
        var recievedInvalidated = grids.eq(1).find('table').eq(0).find('tbody > tr').toArray().map(function (e) {
            var element = $(e);
            var total = parseInt(element.find('td').eq(1).text(), 10);
            var date = element.find('td').eq(2).text();
            return tslib_1.__assign({ Type: 'Invalidated', Given: 0, Recieved: total, Total: 0 }, parseUserDetails(e));
        });
        var givenInvalidated = grids.eq(1).find('table').eq(1).find('tbody > tr').toArray().map(function (e) {
            var element = $(e);
            var total = parseInt(element.find('td').eq(1).text(), 10);
            var date = element.find('td').eq(2).text();
            return tslib_1.__assign({ Type: 'Invalidated', Given: total, Recieved: 0, Total: 0 }, parseUserDetails(e));
        });
        var results = recievedVotes.concat(givenVotes)
            .concat(recievedInvalidated)
            .concat(givenInvalidated);
        // If they don't have any activity with themselves (for example, accepting)
        // Then add in a link to their profile (so we can load their PII)
        if (results.filter(function (r) { return r.UserId === userId; }).length <= 0) {
            results.unshift({
                UserId: userId,
                UserInfo: $("<a href=\"/users/" + userId + "\" style=\"padding: 5px\">" + userName + "</a>"),
                Type: '',
                Given: 0,
                Recieved: 0,
                Total: 0
            });
        }
        // Negate any given/recieved numbers to themselves, distracting and not useful
        results.forEach(function (r) {
            if (r.UserId === userId) {
                r.Given = 0;
                r.Recieved = 0;
                r.Type = '';
                r.Total = 0;
            }
        });
        return {
            Votes: results,
            // Is there the ring of voting graph?
            HasRingleader: $('.ringleader', page).length > 0
        };
    }
    $(setupSockyLink);
    $(function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
        var match;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    match = window.location.href.match(/\/users\/account\-info\/(\d+)#Socky/);
                    if (!match) return [3 /*break*/, 2];
                    return [4 /*yield*/, displaySockyUI(match[1], $('.mod-tabs').text().trim())];
                case 1:
                    _a.sent();
                    _a.label = 2;
                case 2: return [2 /*return*/];
            }
        });
    }); });
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["__extends"] = __extends;
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (immutable) */ __webpack_exports__["__rest"] = __rest;
/* harmony export (immutable) */ __webpack_exports__["__decorate"] = __decorate;
/* harmony export (immutable) */ __webpack_exports__["__param"] = __param;
/* harmony export (immutable) */ __webpack_exports__["__metadata"] = __metadata;
/* harmony export (immutable) */ __webpack_exports__["__awaiter"] = __awaiter;
/* harmony export (immutable) */ __webpack_exports__["__generator"] = __generator;
/* harmony export (immutable) */ __webpack_exports__["__exportStar"] = __exportStar;
/* harmony export (immutable) */ __webpack_exports__["__values"] = __values;
/* harmony export (immutable) */ __webpack_exports__["__read"] = __read;
/* harmony export (immutable) */ __webpack_exports__["__spread"] = __spread;
/* harmony export (immutable) */ __webpack_exports__["__await"] = __await;
/* harmony export (immutable) */ __webpack_exports__["__asyncGenerator"] = __asyncGenerator;
/* harmony export (immutable) */ __webpack_exports__["__asyncDelegator"] = __asyncDelegator;
/* harmony export (immutable) */ __webpack_exports__["__asyncValues"] = __asyncValues;
/* harmony export (immutable) */ __webpack_exports__["__makeTemplateObject"] = __makeTemplateObject;
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = Object.setPrototypeOf ||
    ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
    function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = Object.assign || function __assign(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
    }
    return t;
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r);  }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { if (o[n]) i[n] = function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; }; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator];
    return m ? m.call(o) : typeof __values === "function" ? __values(o) : o[Symbol.iterator]();
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(0)], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, Tools_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var UserActivityHtml = "\n<div id=\"mainbar\" class=\"user-activity\">\n    <form action=\"#\" method=\"get\">\n        <table class=\"options-table\">\n        <tr>\n            <td>\n                <label for=\"fromDate\">Dates</label>\n            </td>\n            <td class=\"data-entry\">\n                <input type=\"text\" name=\"fromDate\" id=\"fromDate\"/>\n                &ndash;\n                <input type=\"text\" name=\"toDate\" id=\"toDate\"/>\n            </td>\n            <td>\n                <input type=\"button\" id=\"refresh\" value=\"refresh\"/>\n            </td>\n        </tr>\n        <tr>\n            <td>\n                <label for=\"addUser\">User</label>\n            </td>\n            <td class=\"data-entry\">\n                <input class=\"user-entry\" type=\"text\" name=\"addUser\" size=\"50\" id=\"user\" />\n            </td>\n            <td>\n                <input id=\"addUser\" type=\"button\" value=\"add\" />\n            </td>\n        </tr>\n        </table>\n    </form>\n\n    <hr />\n\n    <div class=\"compare dno\">\n        <table>\n            <tr>\n                <td>\n                    <table class=\"user-cards\">\n                    </table>\n                </td>\n                <td>\n                    <div class=\"scroller\">\n                        <table class=\"activity-graph\">\n                        </table>\n                    </div>\n                </td>\n            </tr>\n        </table>\n    </div>\n    <div>\n        <div class=\"dots-holder\" style=\"float:left\"></div>\n        <div class=\"permalink\" style=\"float:right\"><a href=\"#\">permalink</a></div>\n    </div>\n\n</div>\n<div id=\"sidebar\" class=\"user-activity\" style=\"float: left;\">\n    <div class=\"module ip-legend dno\">\n        <h4>IP Legend</h4>\n        <div class=\"legend\">\n            No IPs Yet\n        </div>\n    </div>\n</div>\n<br class=\"cbt\">\n";
    function BuildUserActivity(appendTo) {
        appendTo.append(UserActivityHtml);
        Tools_1.AddStyle('https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css');
        Tools_1.AddScript('https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js', function () {
            Tools_1.AddScript('https://cdn.sstatic.net/Js/mod-user-activity.en.js', function () {
                var now = new Date();
                var yesterday = new Date(now - (1000 * 60 * 60 * 24 * 1));
                var then = new Date(now - (1000 * 60 * 60 * 24 * 3));
                var fromDate = then.toISOString().slice(0, 10);
                var toDate = yesterday.toISOString().slice(0, 10);
                userActivity(30, 5, fromDate, toDate);
                $('#fromDate').val(fromDate);
                $('#toDate').val(toDate);
            });
        });
    }
    exports.BuildUserActivity = BuildUserActivity;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ })
/******/ ]);