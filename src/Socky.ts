import * as jquery from 'jquery';
import { BuildUserActivity } from 'UserActivity';
import { AddStyleText, GroupBy, Sum, Delay, AddScript } from 'Tools';

declare const StackExchange: any;

const sockyCss = `
#sockyTable > thead > tr > th {
    background-color: #6a737c;
    color: #FFF;
    border: 1px solid #FFF;
    font-size: 110%;
    padding: 5px;
}

#sockyTable > tbody > tr > td {
    padding: 5px;
    vertical-align: middle;
}
#sockyTable > tbody > tr:nth-child(even) {
    background-color: #3332320d;
}

`;

function setupSockyLink() {
    const match = window.location.href.match(/\/users\/account\-info\/(\d+)/);
    if (match) {
        const userId = match[1];
        $('.mod-quick-links')
            .append(
                $('<li><a href="/users/account-info/' + userId + '#Socky">Socky</a></li>')
                    .click(() => {
                        // If we're already browsing socky, don't trigger a reload.
                        if (!window.location.href.match(/\/users\/account\-info\/(\d+)#Socky/)) {
                            displaySockyUI(userId, $('.mod-tabs').text().trim());
                        }
                    })
            );
    }
}

function addSockyCss() {
    AddStyleText(sockyCss);
    $('.left-sidebar').hide();
    $('#content').css('width', '100%');
    $('#mod-content').css('width', '100%');
    $('#mod-content').css('max-width', '100%');
}

async function displaySockyUI(pageUserId: string, userName: string) {
    const contentBox = $('#mod-content .col-9');
    addSockyCss();

    contentBox.empty();
    const content = $(`
    <div class="subheader" style="height: 100%">
        <h1 style="float: none;">
            Socky information for <a href="/users/${pageUserId}">${userName}</a>
        </h1>
        <h2 id="warnings" style="display: none; float: none; margin-top: 5px; background-color: #efa812; padding: 5px;"></h2>
    </div>
    <table id="sockyTable" style="width: 100%">
        <thead>
            <tr>
                <th><input type="checkbox" id="activitySelectAll"></th>
                <th width="215px">User</th>
                <th>Previous Names</th>
                <th>Real Name</th>
                <th>Email</th>
                <th>IP</th>
                <th>Votes given</th>
                <th>Votes recieved</th>
                <th title="will be logged">PII ⚠</th>
            </tr>
        </thead>
        <tbody id="contentBody">
        </tbody>
    </table>
    <br />
    <input type="button" id="checkUserActivity" value="Check Selected User's Activity" style="cursor: pointer;">
    <br />
    `);

    contentBox.append(content);
    const userIdsToCheck: number[] = [];
    const processAddUser = () => {
        const userIdToCheck = userIdsToCheck.pop();
        if (userIdToCheck) {
            $('#user').val(userIdToCheck);
            $('#addUser').click();
        } else {
            $('#checkUserActivity')
                .removeAttr('disabled')
                .removeClass('disabled-button');
        }
    };

    $('#checkUserActivity').click(() => {
        $('.socky-user-input:checked').each((i, elem) => {
            const aa = parseInt($(elem).attr('data-id'), 10);
            userIdsToCheck.push(aa);
        });
        userIdsToCheck.reverse();
        $('#refresh').click();
        if (userIdsToCheck.length > 0) {
            processAddUser();

            $('#checkUserActivity').disable();
        } else {
            alert('No users selected');
        }
    });

    $('#activitySelectAll').click(() => {
        if ($('#activitySelectAll').prop('checked')) {
            $('.socky-user-input').prop('checked', true);
        } else {
            $('.socky-user-input').prop('checked', false);
        }
    });

    const contentBody = $('#contentBody');
    $.get(`/admin/show-user-votes/${pageUserId}`, async data => {
        const results = parseUserVotes(data, pageUserId, userName);
        const grouped = GroupBy(results.Votes, 'UserId');
        const flattened = flattenUserVotes(grouped, pageUserId);

        const processAfter: (() => void)[] = [];
        for (const item of flattened) {
            const currentUserId = item.UserId;
            if (!currentUserId) {
                continue;
            }

            if (currentUserId === pageUserId) {
                item.UserImage.css('background-color', '#b5f1b5');
            }
            const userImage = $('<td>').append(item.UserImage);
            const previousNames = $('<td>');
            const realName = $('<td>');
            const email = $('<td>');
            const ip = $('<td>');

            const loadPII = $('<a href="javascript:void(0);">Load</a>');
            loadPII.click(async () => {
                loadPII.hide();
                const pii = await FetchPII(currentUserId);

                email.append(pii.email);
                realName.append(pii.realName);

                ip.html(pii.ip);
            });
            contentBody.append(
                $('<tr>')
                    .append(`<td><input type="checkbox" class="socky-user-input" data-id="${currentUserId}"></td>`)
                    .append(userImage)
                    .append(previousNames)
                    .append(realName)
                    .append(email)
                    .append(ip)
                    .append($('<td>').html(item.VotesGiven))
                    .append($('<td>').html(item.VotesRecieved))
                    .append($('<td>').append(loadPII))
            );

            processAfter.push(async () => {
                await Delay(1000);
                const dashboardData = await FetchModDashboard(currentUserId);
                item.UserImage.css('float', 'left');
                dashboardData.modLinks
                    .css('float', 'right')
                    .css('margin-left', '2px')
                    .css('margin-right', '-5px');

                userImage.append(dashboardData.modLinks);

                if (dashboardData.joinedSite) {
                    const joinedDate = $(`<p>Joined ${dashboardData.joinedSite}</p>`).css('margin-bottom', '0px');
                    const userDetailsDiv = item.UserImage.find('.user-details');
                    if (userDetailsDiv.length > 0) {
                        userDetailsDiv.append(joinedDate);
                    } else {
                        item.UserImage.parent().append(joinedDate);
                    }
                }

                dashboardData.pastNames.find('li').each((index, element) => {
                    const newText = $('<p>').text($(element).text());
                    newText.css('margin-bottom', '0px');
                    previousNames.append(newText);
                });
            });
        }

        if (results.HasRingleader) {
            content.find('#warnings')
                .show()
                .html(`Ring voting detected: <a href="/admin/show-user-votes/${pageUserId}">View votes</a>`);
        }

        for (let i = 0; i < processAfter.length; i++) {
            await processAfter[i]();
        }
    });

    BuildUserActivity(contentBox);

    // Fix the permalink for user activity. Without this, it'll point to /users/account-info/userid/#...
    $('.permalink > a').click(() => {
        setTimeout(() => {
            const permalink = $('.permalink input').eq(0);
            const updatedLink = permalink.val().replace(`/users/account-info/${pageUserId}`, '/admin/user-activity');
            permalink.val(updatedLink);
            permalink.select();
        }, 100);
    });

    // The below is watching for the 'add' button to become enabled
    // When it's enabled again, we wait two seconds, and add the next user (if any)

    // https://stackoverflow.com/a/29631624/563532
    const observer = new MutationObserver(mutations => {
        let mutation: any;
        // tslint:disable-next-line:no-conditional-assignment
        for (let i = 0; mutation = mutations[i]; i++) {
            if (mutation.attributeName === 'disabled') {
                if (!mutation.target.disabled) {
                    setTimeout(() => {
                        processAddUser();
                    }, 2000);
                }
            }
        }
    });

    // Observe attributes change
    observer.observe($('#addUser').get(0), { attributes: true });

    const reputationInfo = await FetchUserReputation(pageUserId);
    contentBox.append(reputationInfo.Content);
}

interface PII {
    email: JQuery;
    realName: JQuery;
    ip: string;
}
function FetchPII(userId: string) {
    return new Promise<PII>((resolve, reject) => {
        $.ajax({
            url: `/admin/all-pii`,
            type: 'POST',
            data: { fkey: (StackExchange as any).options.user.fkey, id: userId },
            success: (pii) => {
                const data = $('.col-4', pii);
                const email = data.eq(0).find('a');
                email.remove('svg');

                const realName = data.eq(1).find('a');
                realName.remove('svg');
                const ip = data.eq(2).html();

                resolve({
                    email,
                    realName,
                    ip
                });
            },
            error: (err) => {
                reject(err);
            }
        });
    });
}

interface ModDashboardData {
    modLinks: JQuery;
    pastNames: JQuery;
    joinedSite: string;
}

function FetchModDashboard(userId: string) {
    return new Promise<ModDashboardData>((resolve, reject) => {
        $.get(`/users/account-info/${userId}`, data => {
            const jqueryPage = $(data);
            const results = parseModDashboard(jqueryPage);
            resolve(results);
        }).fail(err => {
            reject(err);
        });
    });
}

function parseModDashboard(page: JQuery) {
    const modLinks = $('<span class="mod-links"></span>').append(page.find('.mod-flag-indicator'));
    const pastNames = page.find('.col-2:contains("Past names:")').next().find('ul');
    const joinedSite = page.find('.col-2:contains("Joined site:")').next().text();
    return { modLinks, pastNames, joinedSite };
}

function FetchUserReputation(userId: string) {
    let capturedResolve: any;
    let capturedReject: any;
    const promise = new Promise<{ Content: JQuery }>((resolve, reject) => {
        capturedResolve = resolve;
        capturedReject = reject;
    });
    AddScript('https://cdn.sstatic.net/Js/third-party/highcharts.js', () => {
        $.get(`/users/${userId}/?tab=reputation`, data => {
            const jqueryPage = $(data);
            const results = parseReputationPage(jqueryPage);
            capturedResolve(results);
        }).fail(err => {
            capturedReject(err);
        });
    });

    return promise;
}

function parseReputationPage(page: JQuery) {
    return { Content: page.find('#user-tab-reputation .user-tab-content') };
}

function flattenUserVotes(grouped: any, userId: string) {
    const flattened = Object.keys(grouped)
        .map(key => {
            const values = grouped[key] as UserVotesInfo[];
            const userImage = values[0].UserInfo;
            const votingUserIds = values.filter(v => !!v.UserId);
            const votingUserId =
                votingUserIds.length ?
                    votingUserIds[0].UserId
                    : null;

            const votesGiven = values.filter(v => v.Given > 0)
                .reduce((previous, current, i) => {
                    const voteText = current.Type !== 'Invalidated'
                        ? current.Given + ' (' + (current.Total === 0 ? 0 : Math.round(current.Given * 100 / current.Total)) + '%) '
                        : current.Given + ' '
                        ;
                    return previous + voteText + current.Type + (i === 0 ? '<br />' : '');
                }, '');
            const votesRecieved = values.filter(v => v.Recieved > 0)
                .reduce((previous, current, i) => {
                    const voteText = current.Type !== 'Invalidated'
                        ? current.Recieved + ' (' + (current.Total === 0 ? 0 : Math.round(current.Recieved * 100 / current.Total)) + '%) '
                        : current.Recieved + ' '
                        ;

                    return previous + voteText + current.Type + (i === 0 ? '<br />' : '');
                }, '');

            return {
                UserImage: userImage,
                UserId: votingUserId,
                PreviousName: null,
                RealName: null,
                Email: null,
                IP: null,
                VotesGiven: votesGiven,
                VotesRecieved: votesRecieved,
                TotalVotes: Sum(values, v => v.Given + v.Recieved),
            };
        });

    flattened.sort((a, b) => {
        if (a.UserId === userId) { return -1; }
        if (b.UserId === userId) { return 1; }
        return b.TotalVotes - a.TotalVotes;
    });
    return flattened;
}

interface UserVotesInfo {
    Type: string;
    Recieved: number;
    Given: number;
    Total: number;
    UserId: string | null;
    UserInfo: JQuery;
}
function parseUserVotes(page: string, userId: string, userName: string): { Votes: UserVotesInfo[], HasRingleader: boolean } {
    const grids = $('#mainbar-full > .d-flex', page);

    const parseUserDetails = (e: HTMLElement) => {
        const element = $(e);
        const userInfo = element.find('.user-info');
        let currentUserId: string | null = element.attr('data-user-id');
        if (currentUserId === undefined) {
            // Deleted sock puppets don't have a data-user-id attribute
            const userLink = userInfo.find('.user-details > a').attr('href');
            const userIdMatch = userLink.match(/\/users\/(\d+)/);
            currentUserId = userLink && userIdMatch ? userIdMatch[1] : null;
        }
        return {
            UserId: currentUserId,
            UserInfo: userInfo
        };
    };

    const recievedVotes =
        grids.eq(0).find('table').eq(0).find('tbody > tr').toArray().map(
            e => {
                const element = $(e);
                const numColumns = element.find('td').length;

                const type = element.find('td').eq(numColumns === 5 ? 1 : 0).find('span').text();
                const votes = element.find('td').eq(numColumns === 5 ? 2 : 1).text();
                const recieved = parseInt(votes.split(' / ')[0], 10);
                const total = parseInt(votes.split(' / ')[1], 10);

                return {
                    Type: type,
                    Recieved: recieved,
                    Given: 0,
                    Total: total,
                    ...parseUserDetails(e)
                };
            }
        );

    const givenVotes =
    grids.eq(0).find('table').eq(1).find('tbody > tr').toArray().map(
            e => {
                const element = $(e);
                const numColumns = element.find('td').length;

                const type = element.find('td').eq(numColumns === 5 ? 1 : 0).find('span').text();
                const votes = element.find('td').eq(numColumns === 5 ? 2 : 1).text();
                const given = parseInt(votes.split(' / ')[0], 10);
                const total = parseInt(votes.split(' / ')[1], 10);

                return {
                    Type: type,
                    Recieved: 0,
                    Given: given,
                    Total: total,
                    ...parseUserDetails(e)
                };
            }
        );

    const recievedInvalidated =
        grids.eq(1).find('table').eq(0).find('tbody > tr').toArray().map(
            e => {
                const element = $(e);
                const total = parseInt(element.find('td').eq(1).text(), 10);
                const date = element.find('td').eq(2).text();
                return {
                    Type: 'Invalidated',
                    Given: 0,
                    Recieved: total,
                    Total: 0,
                    ...parseUserDetails(e)
                };
            }
        );

    const givenInvalidated =
        grids.eq(1).find('table').eq(1).find('tbody > tr').toArray().map(
            e => {
                const element = $(e);
                const total = parseInt(element.find('td').eq(1).text(), 10);
                const date = element.find('td').eq(2).text();
                return {
                    Type: 'Invalidated',
                    Given: total,
                    Recieved: 0,
                    Total: 0,
                    ...parseUserDetails(e)
                };
            }
        );

    const results =
        recievedVotes.concat(givenVotes)
            .concat(recievedInvalidated)
            .concat(givenInvalidated);

    // If they don't have any activity with themselves (for example, accepting)
    // Then add in a link to their profile (so we can load their PII)
    if (results.filter(r => r.UserId === userId).length <= 0) {
        results.unshift({
            UserId: userId,
            UserInfo: $(`<a href="/users/${userId}" style="padding: 5px">${userName}</a>`),
            Type: '',
            Given: 0,
            Recieved: 0,
            Total: 0
        });
    }

    // Negate any given/recieved numbers to themselves, distracting and not useful
    results.forEach(r => {
        if (r.UserId === userId) {
            r.Given = 0;
            r.Recieved = 0;
            r.Type = '';
            r.Total = 0;
        }
    });
    return {
        Votes: results,

        // Is there the ring of voting graph?
        HasRingleader: $('.ringleader', page).length > 0
    };
}

$(setupSockyLink);
$(async () => {
    const match = window.location.href.match(/\/users\/account\-info\/(\d+)#Socky/);
    if (match) {
        await displaySockyUI(match[1], $('.mod-tabs').text().trim());
    }
});
