// https://stackoverflow.com/a/8578840/563532
export function AddScript(url: string, callback?: () => void) {
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.async = true;
    script.onload = () => {
        // remote script has loaded
        if (callback) {
            callback();
        }
    };
    script.src = url;
    document.getElementsByTagName('head')[0].appendChild(script);
}

export function AddStyle(url: string) {
    const head = document.head || document.getElementsByTagName('head')[0];
    const style = document.createElement('link');
    style.rel = 'stylesheet';
    style.type = 'text/css';
    style.href = url;
    head.appendChild(style);
}

export function AddStyleText(text: string) {
    const head = document.head || document.getElementsByTagName('head')[0];
    const style = document.createElement('style');

    style.type = 'text/css';
    if ((style as any).styleSheet) {
        (style as any).styleSheet.cssText = text;
    } else {
        style.appendChild(document.createTextNode(text));
    }
    head.appendChild(style);
}

export function Sum<T>(data: T[], selector: (element: T) => number): number {
    return data.reduce((previous, current) => previous + selector(current), 0);
}
export function Max<T>(data: T[], selector: (element: T) => number): number | null {
    const result = data.reduce((previous, current) => {
        const currentValue = selector(current);
        if (previous === undefined || previous < currentValue) {
            return previous;
        }
        return currentValue;
    }, undefined as number | undefined);
    if (result === undefined) {
        return null;
    }
    return result;
}

// https://stackoverflow.com/a/34890276/563532
export function GroupBy(data: any[], key: string) {
    return data.reduce((rv, x) => {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {});
}

export function Delay(time: number) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve();
        }, time);
    });
}
