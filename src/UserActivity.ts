import { AddScript, AddStyle } from 'Tools';

declare const userActivity: any;
const UserActivityHtml = `
<div id="mainbar" class="user-activity">
    <form action="#" method="get">
        <table class="options-table">
        <tr>
            <td>
                <label for="fromDate">Dates</label>
            </td>
            <td class="data-entry">
                <input type="text" name="fromDate" id="fromDate"/>
                &ndash;
                <input type="text" name="toDate" id="toDate"/>
            </td>
            <td>
                <input type="button" id="refresh" value="refresh"/>
            </td>
        </tr>
        <tr>
            <td>
                <label for="addUser">User</label>
            </td>
            <td class="data-entry">
                <input class="user-entry" type="text" name="addUser" size="50" id="user" />
            </td>
            <td>
                <input id="addUser" type="button" value="add" />
            </td>
        </tr>
        </table>
    </form>

    <hr />

    <div class="compare dno">
        <table>
            <tr>
                <td>
                    <table class="user-cards">
                    </table>
                </td>
                <td>
                    <div class="scroller">
                        <table class="activity-graph">
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div>
        <div class="dots-holder" style="float:left"></div>
        <div class="permalink" style="float:right"><a href="#">permalink</a></div>
    </div>

</div>
<div id="sidebar" class="user-activity" style="float: left;">
    <div class="module ip-legend dno">
        <h4>IP Legend</h4>
        <div class="legend">
            No IPs Yet
        </div>
    </div>
</div>
<br class="cbt">
`;
export function BuildUserActivity(appendTo: JQuery) {
    appendTo.append(UserActivityHtml);
    AddStyle('https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css');
    AddScript('https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js', () => {
        AddScript('https://cdn.sstatic.net/Js/mod-user-activity.en.js', () => {
            const now = new Date();
            const yesterday = new Date((now as any) - (1000 * 60 * 60 * 24 * 1));
            const then = new Date((now as any) - (1000 * 60 * 60 * 24 * 3));

            const fromDate = then.toISOString().slice(0, 10);
            const toDate = yesterday.toISOString().slice(0, 10);

            userActivity(30, 5, fromDate, toDate);
            $('#fromDate').val(fromDate);
            $('#toDate').val(toDate);
        });
    });
}
